# [HTML Questions](https://github.com/hackreactor/front-end-interview-handbook/blob/master/contents/en/html-questions.md)

## What is a DocType?
- Abbreviation for DOCument Type (always associated to a DTD - Document Type Definition)
    - DTD defines how documents of a type are to be structured (button can only have a span and not a div)
    - Doctype declares what DTD a document respects (e.g. HTML DTD)
        - For webpages, DOCTYPE declaration is req and tells user agents what version of HTML specifications doc respects
        - Once a user  recognized correct DOCTYPE, trigger no-quirks mode matching DOCTYPE for reading document
        - If user agent does not recognize a correct DOCTYPE, it triggers the quirks mode
        - DOCTYPE declaration for HTML5 = <!DOCTYPE html>
    - References
        - [DOC-Type](https://html.spec.whatwg.org/multipage/syntax.html#the-doctype)
        - [XHTML](https://html.spec.whatwg.org/multipage/xhtml.html)
        - [Quirks](https://quirks.spec.whatwg.org/)

## Serve a page w/ content in several languages?
- When HTTP request is made to server, requesting user agent sends info about language preferences like in Accept-Language header
    - sever can use this info to return ver of document in req language, if an alt is available
    - html doc should declare lang attribute in `<html lang='en'>...</html>` tag
- Must make use of hreflang attributed in <head> for search engines
    - `<link rel='alternate' hreflang='de' href='http://de.example.com/page.html'/>`
- in BackEnd, html markup contains `i18n` placeholders, content for specific language stored in YML, JSON formats -> server dynamically gens HTML page w/ content in language, with help of BE framework



















<!-- <details>
<summary>
    Details and Summary
</summary>

- Data
</details> -->
