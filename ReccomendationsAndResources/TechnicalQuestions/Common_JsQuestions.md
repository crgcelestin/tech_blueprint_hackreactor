# [JS Questions](https://github.com/hackreactor/front-end-interview-handbook/blob/master/contents/en/javascript-questions.md)

## Event Delegation
- Technique that involves adding event listeners to parent element vs adding them to descendant elements __->__ Listener fires when event is triggered on descendant elements as a result of event bubbling (DOM)
    - Memory footprint is mitigated when only one single handler is req on parent element as opposed to being attached to event handlers on each descendant
    - No need to unbind handler from elements removed, bind event for new elements
        - [Event Delegation](https://davidwalsh.name/event-delegate)
            -
        - [Dom Event Delegation](https://stackoverflow.com/questions/1687296/what-is-dom-event-delegation)
            -

## How'this' works in Javascript
- 1. if new keyword is used when calling a function, 'this' inside function is a new object
- 2. if apply, call, bind are used to call/create a fxn, 'this' inside fxn is obj passed in as an arg
- 3. if fxn is called as method like obj.method(), this = obj that function is a property of
- 4. if fxn invoked as free function invocation i.e. invoked w/o any above conditions, this = global object
    - in browser, this = window obj, in strict mode: this is undefined
- Example of this in ES6
    - Allows one to use arrow fxns using enclosing lexical scope
    - Prevents caller from controlling context via .call or .apply
