# [10 JS Questions](https://medium.com/javascript-scene/10-interview-questions-every-javascript-developer-should-know-6fa6bdf5ad95)

## Notes
> Content of Recent Job Interviews by Hiring Manager
> - __Ask Candidate to Build Click Counter__
>   - Keep track of number of times that user clicks button during a session. No storage. No network I/O. Just click counting. -> verify ability to code
> - __List of Questions__
>   - General programming concepts like
    - [Closures](https://medium.com/javascript-scene/master-the-javascript-interview-what-is-a-closure-b2f0d2152b36)
    - [Promises](https://medium.com/javascript-scene/master-the-javascript-interview-what-is-a-promise-27fc71e77261)
> - Functional programming dom over class inheritance in JS
>  - Libraries and frameworks: React, Redux, RxJS, Lodash, Ramda dominate over inheritance libraries, frameworks. Focus should be on familiarity with architecture and paradigms
> -

### 1. Can you name two programming paradigms important for JavaScript app developers?

- JS is a  multi-paradigm language that supports imperative and procedural programming w/ OOP (Object-Oriented Programming), Functional Programming
    - Supports OOP w/ prototypal inheritance
    > Good to hear
    > - Prototypal Inheritance, Functional Programming (closures, first class functions, lambdas)
    - [Prototypal OO](https://medium.com/javascript-scene/the-two-pillars-of-javascript-ee6f3281e7f3)
    - [Functional Programming](https://medium.com/javascript-scene/the-two-pillars-of-javascript-pt-2-functional-programming-a63aa53a41a4)

### 2. What is functional programming?
- Produces programs by composing math functions, avoids shared state, mutable data
- Essential JS concept, with several common functional utilities added to JS in ES5
    > - Mention: Pure functions/function purity, avoid side effects, simple function composition, mention fts supporting FP  (first class functions, high order functions, functions as arguments/values)
    - [Immutability](https://medium.com/javascript-scene/the-dao-of-immutability-9f91a70c88cd)
    - [Software Composition](https://medium.com/javascript-scene/composing-software-an-introduction-27b72500d6ea)

### 3. What is the difference between classical inheritance and prototypal inheritance?

### Running a Black JS File
> ![Blank JS](Img/Blank_JS_Question.PNG)

> If ran in browser, JS creates 3 things by default: 2 that can be accessed in browser's console.
> What are the 3 things and what 2 are accessible in console ?

![Answer](Img/Blank_Answer.jpg)
- Window object and Execution Context are created and are able to be accessed in console
- This keyword is not accessed via console but is created
