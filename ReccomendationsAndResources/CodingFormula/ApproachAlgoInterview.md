# [Approach Algo Interview](https://docs.google.com/document/d/1ZWOEiNu6TvPwz_EVWTCplCrGjnbT-oIdv6b-eB7cRCU/edit)

### [Problem Discussed: String to Integer](https://leetcode.com/problems/string-to-integer-atoi/)

## 1 - Rephrase Problem in Own Words
- Ask interviewer questions to clarify requirements and parse out crucial information
- Rewrite problem into own words to form a mental model and digest problem

- For problem, 1 question asked is if one is allowed to use type casting in order to convert one char at a time
```js
//other questions upon rephrasing or ideation of a naive/bruteforce approach

/*

    given a string, return number value
    ignore white space at string start
    number begins with - or +
    all chars after number are to be ignored
    string is invalid if char that is not a white space, sign comes before number
    if string has no integer values, it is invalid
    return value for any invalid string is -
    resulting integer can't be larger than (2^31)-1 or smaller than -(2^31)

*/
```
^^ From these req, algo will require looping and conditional logic - still too early to formulate any concrete plans

! CLARIFYING QUESTIONS !
## 2 - Input and Output Types
- Make sure to get input, outputs of algo - code comment
    - 2 Functions
        - 1. Solidifies what parameters of function will be, what signature will appear to be
        - 2. Allows for a reminder of the types that will be worked with
    - For String to Integer Problem
    ```js
    Input: stringOutput: 32-bit signed integerSignature: myAtoi(str)
    ```

! CLARIFYING QUESTIONS !
## 3 - Examples and Edge Cases
- Now confident of inputs and outputs, come up with tests cases
- Examples need to cover all potential edge cases possible
    - Potential code may miss a singular edge case and cause solution to fall apart
    - Come up with some that your interviewer may not even provide

```js
//test cases
var myAtoi = (s) =>{

}
console.log(42==myAtoi('42'))
console.log(-42=myAtoi('-42'))
console.log(4193==myAtoi('4193 with words'))
console.log(0==myAtoi('words and 987'))
console.log(-2147483648==myAtoi('-91283472332'))

/*
    Additional Possibilities
    Need to consider a number starting with a +
    Or if multiple signs come before the number

*/
console.log(50==myAtoi('+50.890'))
console.log(0==myAtoi('-+100'))
console.log(0==myAtoi('!another invalid -10'))
```

## 4 - Data Structures
- Critical to consider is which data structure(s) will be used and their collective impact on implementation
    - Will another data structure help with type conversion

- One issue already with the problem is keeping track of places of each digit, can use an array to keep track of integer chars -> array serves as interim placeholder for each char prior to conversion into final integer

- Create a naive solution prior to prioritization of space efficient solutions

## 5 - Pseudocode
- Lay out algo in pseudocode as interviewers want to see how one thinks and approaches problems

- Interviewer will know how to assist if an action plan is created

```
    Example of action plan (pseudcode) for this string-integer problem:
    (1) start with 0 index (index = 0)
    (2) while char at current index = white space (str[index]=" ")
        (index increment)
    (3) if next char is invalid, return 0
    (4) if next char is positive or negative sign
        (a) if negative, mark number as negative
        (b) if positive, mark number as positive
        (index increment)
    (5) loop through chars starting at current index,
        if current char is integer
            unshift into front of array
            increment index
        else
            break out of loop
    (6) loop through string integer array
        cast string char into integer
        multiply integer by 10^index and add to return value
    (7) if string includes a negative sign
            multiply result value by -1
        if result value is < min, reassign to min
        if result value is > max, reassign to max
    (8) return value
```
- Read over reqs, inputs/outputs, edge cases and ask questions, clarifying concepts, isolate areas of uncertainty
    - Find simplest solution and develop from first iteration
    - Require DFS, Sliding Window, DC


## 6 - Code
- If interviewer provides 45 minutes, spend 15-30 minutes thinking and mentally digesting the problem
- All heavy work has been done as it pertains to coding -> Interpret mental model into code
- Several factors influence - time, responsiveness of interviewer

### Problem Documentation
#### Code

#### __Problem Instructions__

Implement the myAtoi(string s) function, which converts a string to a 32-bit signed integer (similar to C/C++'s atoi function).

The algorithm for myAtoi(string s) is as follows:

Read in and ignore any leading whitespace.
[" 32"]

Check if the next character (if not already at the end of the string) is '-' or '+'. Read this character in if it is either. This determines if the final result is negative or positive respectively. Assume the result is positive if neither is present.
["-32", "+32"]

Read in next the characters until the next non-digit character or the end of the input is reached. The rest of the string is ignored.
["-32a", "+b32", "3a 2"]

Convert these digits into an integer (i.e. "123" -> 123, "0032" -> 32). If no digits were read, then the integer is 0. Change the sign as necessary (from step 2).
[if " " then it is 0]

If the integer is out of the 32-bit signed integer range [-2^31, 2^31 - 1], then clamp the integer so that it remains in the range. Specifically, integers less than -231 should be clamped to -2^31, and integers greater than 2^31 - 1 should be clamped to 2^31 - 1.
[set and upper and lower bound limits, number must be within range of -2^31, 2^31-1 and clamped]

Return the integer as the final result.

Note:

Only the space character ' ' is considered a whitespace character.

Do not ignore any characters other than the leading whitespace or the rest of the string after the digits.

Constraints:
0 <= s.length <= 200
s consists of upper, lower case English letters, digits, '','+','-','.'
