# Preparing and Interview Strategies - Medium Articles

## [Part 2 - Prep](https://medium.com/@gourikabang1996/part-2-how-to-prepare-for-your-next-coding-interview-d048b188301e)

### Notes
- __General Format of Coding Interview__
    - (1) Intro
        - Introductions, explanation of interviewer's work
    - (2) Career Aspiration
        - Interviewer asks questions about background regarding experience, career aspirations
            - Prep elevator pitch ['tell me about self'](https://theinterviewguys.com/tell-me-about-yourself-interview-question/)
                - [Tailoring](TailorSelf.md)
            - Prepare to talk about non coding topics (employment, career goals, past projects)
            - Interests and strengths as an engineer
            - Soft skills, passion
    - (3) Coding
        - IDE or whiteboard, with 1 or more questions (solve in 10 to 30 minutes) and allows way to understand problem solving approach
        - Further constraints or reqs can be added
    - (4) Questions
        - Ask questions regarding company research
        - Tell more about interests

- __Study Content__

    - Trees
        -  Understand construction, conversion, traversal, manipulation algos
        - Comfort in binary trees, BSTs, Trie related questions
        - N-ary trees and be aware of balanced Tree versions (AVL or red black tree)
        - Review both recursive and iterative approaches
        - Serialization and deserialization of trees
        - Know if question is regarding Binary Tree or BST (Binary Search Tree)
            - Finding LCA (Lowest Common Ancestor)

    - Graphs
        - Graphs req to represent relationships
            - Most fundamental and flex way of rep relationships
        - Can a problem be applied with graph algo being distance, search, connectivity, cycle-detection
        - Familiarize self with graph representation (adjacency list v matrix)
        - Understand basic graph traversal algos: BFS, DFS
            - Understand computational complexities, tradeoffs, real world implementation
        - Study up on Dijkstra, Prim algos
        - Topological sorting, cycle detection
            - [Topological Sort Question](https://www.geeksforgeeks.org/given-sorted-dictionary-find-precedence-characters/)
        - STL (built in ds structures) for ease

    - Hash Tables : Commonly Used DS for algo questions
        -  Classic methods to mitigate time complexity is using more space
        - Amortized TC of insertion and deletion is O(1) and can be combined with other DS to achieve efficient TC across various operations
            - [LRU Cache](https://www.geeksforgeeks.org/lru-cache-implementation/)
        - Critical to understand difference between Hash tables and BST
            - [Hashing](https://www.geeksforgeeks.org/hashing-set-1-introduction/)
            - [Hashing DS](https://www.geeksforgeeks.org/hashing-data-structure/)
        - Should be able to implement hash table using arrays in pref language and if it is not the main concern use sets, maps to achieve same functionality
        - Problems involving Hash Tables
            - [Pair with Sum X](https://www.geeksforgeeks.org/given-an-array-a-and-a-number-x-check-for-pair-in-a-with-sum-as-x/)
            - [Subarray with sum 0](https://www.geeksforgeeks.org/find-if-there-is-a-subarray-with-0-sum/)

    - Arrays and Strings
        - Study array rotation, rearrangement, optimization problems
        - Palindromes, Anagrams, Parentheses: Be aware of how to check if a string holds these properties, how to extend strings with min number of insertions until they have any of these properties
        - Subsequence and subarray problems (perform enough of these problems until comfort)
        - Critical Problems
            - [Minimum window substring](https://www.geeksforgeeks.org/find-the-smallest-window-in-a-string-containing-all-characters-of-another-string/)
            - [Max sum subarray](https://www.geeksforgeeks.org/largest-sum-contiguous-subarray/)
            - [Max subsequence sum such that no 3 are consecutive](https://www.geeksforgeeks.org/maximum-subsequence-sum-such-that-no-three-are-consecutive/)
            - [Kth largest element](https://www.geeksforgeeks.org/kth-smallestlargest-element-unsorted-array-set-2-expected-linear-time/)

    - Recursion and BackTracking
        - Several coding problems involve thinking recursively, potentially coding a recursive solution
            - Use recursion to find more elegant problem solutions
                -  Classic examples: [Word Break](https://www.geeksforgeeks.org/word-break-problem-using-backtracking/), [Boggle](https://www.geeksforgeeks.org/boggle-find-possible-words-board-characters/)

    - Memoization and Greediness
        - Practice Dynamic Programming problems especially tabulation
        - DP Problems:
            - [LCS](https://www.geeksforgeeks.org/longest-common-subsequence/), [LIS](https://www.geeksforgeeks.org/longest-increasing-subsequence/), [Min Cost Path](https://www.geeksforgeeks.org/min-cost-path-dp-6/), [0-1 Knapsack](https://www.geeksforgeeks.org/palindrome-partitioning-dp-17/), [Palindrome partitioning](https://www.geeksforgeeks.org/palindrome-partitioning-dp-17/)
        - Greedy Algo Problems:
            - [Activity Selection](https://www.geeksforgeeks.org/activity-selection-problem-greedy-algo-1/), [Maximize difference in circular array](https://www.geeksforgeeks.org/maximize-sum-consecutive-differences-circular-array/), [K bookings](https://www.geeksforgeeks.org/find-k-bookings-possible-given-arrival-departure-times/), [Fractional Knapsack](https://www.geeksforgeeks.org/fractional-knapsack-problem/)

    - Big O Notation
        - Asked to determine TC/SC as well as how to optimize for better TC/SC with given complexity guidelines
            -  While in practice, perform analysis and add time and space complexities of written code
            - To improve speed, choose to either use an appropriate DS or algo or use more memory - classic space and time trade off

    - Searching and Sorting
        - Binary Search is a crucial algo, If an array is sorted and log(n) algo is expected think about BST as opposed to linear search for a non sorted array
        - Sorting = common approach for optimization problems
            - Know details of Merge and Quick Sort as well as differences
        - Be comfortable with DC paradigm
            - [Two sorted arrays median](https://www.geeksforgeeks.org/median-of-two-sorted-arrays/)
        - Be comfortable with sorting functions and what type of input data they are efficient on or not
            - Efficiency in terms of runtime, utilized space
            - In exceptional cases, insertion sort and radix are better as compared to quick, merge, heapsort

    - Linked Lists
        - Practice on loop detection, cloning, flattening, union, intersection and basic manipulation
        - Read on circular and doubly linked lists
        - Don't attempt sorting of linked list in interview

    - Stacks and Queues
        - Array and List implementation of Stacks and Queues
        - Problems:
            - [Queue using Stacks](https://www.geeksforgeeks.org/queue-using-stacks/), [Stack using Queues](https://www.geeksforgeeks.org/implement-stack-using-queue/), [K stacks in an array](https://www.geeksforgeeks.org/efficiently-implement-k-stacks-single-array/)
        - Problems related to stack
            - [Stock Span](https://www.geeksforgeeks.org/the-stock-span-problem/), [Celebrity Problem](https://www.geeksforgeeks.org/the-celebrity-problem/), [Balanced Parentheses](https://www.geeksforgeeks.org/check-for-balanced-parentheses-in-an-expression/), [NGE](https://www.geeksforgeeks.org/next-greater-element/), [Histogram](https://www.geeksforgeeks.org/largest-rectangle-under-histogram/)

    - Heaps
        - Use a heap anytime you want to keep track of 'top' or 'min' in list of size n
        - Be comfortable with Priority Queue
            - [Median of Integer Stream](https://www.geeksforgeeks.org/median-of-stream-of-running-integers-using-stl/)

    - __Others__
        - Study famous classes of NP complete problems
            - Traveling Salesman, Knapsack
        - Understand basic discrete math problems, probability
        - Read about stack and heap memory management
        - Operating sys concepts: process v threads, concurrency issues, semaphores, locks, context switching, process scheduling
        - Bit Manipulation

- Resources
    - [Interview Bit](https://www.interviewbit.com/courses/programming/)
    - HackerRank
    -
- Tips
    - With a preferred language, understand the built-in implementation of various DS allowing for awareness of existing DS in order to select appropriate structures to tackle problem
    - Understand TC, SC of common ops in chosen language
    - Sort() TC, SC
    - Understanding how augmentation of DS can bbe used to achieve efficient TC across various operations
        - HashMap can be used with a doubly LL to achieve O(1) TC for get, put ops in LRU Cache
    - If stuck on problem, enumerate through ds and consider whether each can be applied to problem
    - Recruiter
        - Ask recruiter about format, expectations, prep material, general tips

- Week before Interview
    - Brush up on bookmarked links
    - Perform more research on company regarding team, culture fit, position
    - Polish soft skills, understanding of specific tech topics
    - Find company specific Questions
        - [CareerCup](http://www.careercup.com/page)
- Reading
    - [What does a SWE Interview Entail](https://steve-yegge.blogspot.com/2008/03/get-that-job-at-google.html)
    - [Get a Facebook Job](https://www.facebook.com/notes/facebook-engineering/get-that-job-at-facebook/10150964382448920)
    - [30 Minute Guide](https://www.freecodecamp.org/news/coding-interviews-for-dummies-5e048933b82b/)
    - [Ace Coding Interview Patterns](https://hackernoon.com/14-patterns-to-ace-any-coding-interview-question-c5bb3357f6ed)

## [Part 3 - Cracking Tech Interview](https://medium.com/@gourikabang1996/part-3-crack-a-coding-question-in-a-tech-interview-heres-how-667cb2abda67)
- What interviewers are looking for in the coding challenge?
 - Read and analyze Problem
 - Clarify
 - Explanation
 - Green Light -> Readable and Well Factored Code
 - Find Bugs
 - Test Code

![Interview Plan](Img/Interview_Cheat.jpg)


## [Part 4 - Prep for Behavioral](https://medium.com/@gourikabang1996/part-4-interviewing-is-not-just-coding-prepping-for-behavioral-afe68f4762dc)

![Behavioral Plan](Img/Behaviorial_Cheat.jpg)
